FROM rust:slim as builder


# not expecting a cache mechanism like the one in buildx, the base image includes
# this config file that essentially purges the cache on every install operation
# which is why we have to remove this config to take advantage of the host's cache
RUN rm /etc/apt/apt.conf.d/docker-clean

# libssl-dev is a build dependency for cargo-audit
RUN \
    # holds the package _indexes_
    --mount=type=cache,target=/var/lib/apt/lists \
    # holds the package _contents_
    --mount=type=cache,target=/var/cache/apt/archives \
    buildDeps='pkg-config libssl-dev' \
    && apt update \
    && apt install -y --no-install-recommends $buildDeps \
    && apt autoremove

# apk add openssl openssl-dev pkgconfig libc-dev

ARG CARGO_TARGET_DIR=/tmp/rustybuild

# should trigger an index update, allowing us to cache this layer for the
# following step
RUN --mount=type=cache,target=/usr/local/cargo/registry \
    cargo search --limit 0

RUN --mount=type=cache,target=/usr/local/cargo/registry \
    --mount=type=cache,target=$CARGO_TARGET_DIR \
    cargo install junitify cargo-audit

######################################################################################

FROM rust:slim

# should trigger an index update, allowing us to cache this layer during build
RUN cargo search --limit 0

COPY --from=builder /usr/local/cargo/bin/cargo-audit /usr/local/cargo/bin/
COPY --from=builder /usr/local/cargo/bin/junitify /usr/local/cargo/bin/

# this is luckily quite a clean process, no magic needed
RUN rustup component add rustfmt
